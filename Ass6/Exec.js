// Generated by CoffeeScript 2.3.2
(function() {
  var ClassB, ClassC, b, c;

  ClassB = require("./ClassB");

  ClassC = require("./ClassC");

  b = new ClassB;

  c = new ClassC;

  b.callA();

  c.callA();

}).call(this);
