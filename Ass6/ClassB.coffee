ClassA = require "./ClassA"

module.exports = class ClassB
    constructor: () ->
    callA: ->
        a = new ClassA
        a.add()
        console.log("ScriptB: #{a.x}")