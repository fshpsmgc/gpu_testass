x = 0
add = () -> 
    ++x
    console.log("A: adding one to x: #{x}")

module.exports = 
    x:x
    add:add
    